libnet-pcap-perl (0.21-2) unstable; urgency=medium

  * Remove created test binary via debian/clean. (Closes: #1047261)
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Mar 2024 18:11:23 +0100

libnet-pcap-perl (0.21-1) unstable; urgency=medium

  * Import upstream version 0.21.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Feb 2023 20:07:17 +0100

libnet-pcap-perl (0.20-1) unstable; urgency=medium

  * Import upstream version 0.20.
  * Drop spelling.patch, applied upstream.

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 Dec 2021 16:43:29 +0100

libnet-pcap-perl (0.19-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.4.1, no changes needed.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.19.
  * Drop Net-Pcap-0.18-Adapt-a-test-to-libpcap-1.8.0.patch, fixed
    upstream.
  * Drop Net-Pcap-0.18-Fix-build-with-libpcap-1.9.0.patch, fixed upstream.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.

 -- gregor herrmann <gregoa@debian.org>  Fri, 03 Dec 2021 01:50:02 +0100

libnet-pcap-perl (0.18-3) unstable; urgency=medium

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * Add patch from Fedora for libpcap 1.9.0 compatibility.
    (Closes: #932282)
  * Update years of packaging copyright.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove trailing whitespace from debian/*.

 -- gregor herrmann <gregoa@debian.org>  Fri, 19 Jul 2019 16:53:15 -0300

libnet-pcap-perl (0.18-2) unstable; urgency=medium

  * Add patch from CPAN RT for libpcap 1.8 compatibility.
    (Closes: #843704)
  * Add build dependency on libtest-exception-perl to enable more tests.
  * Add a patch to fix spelling mistakes in the POD.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Nov 2016 17:46:36 +0100

libnet-pcap-perl (0.18-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Add debian/upstream/metadata
  * Import upstream version 0.18
  * Drop patches:
    - 0001-Append-CCFLAGS-to-Config-ccflags-instead-of-overridi.patch
    - spelling-errors.patch
    - fail-without-libpcap.patch
  * Update copyright years for upstream files
  * Declare compliance with Debian policy 3.9.8
  * debian/rules: Enable the bindnow hardening flag
  * Declare package as autopkgtestable

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 22 May 2016 14:18:32 +0200

libnet-pcap-perl (0.17-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Xavier Guimard ]
  * Imported Upstream version 0.17
  * Update debian/copyright (years and format)
  * Bump Standards-Version to 3.9.4
  * Bump debhelper compatibility to 9 to get hardening flags
  * Remove safe_signals patch now included in upstream
  * Add spelling patch (reported to RT)
  * Refresh ccflags patch

  [ gregor herrmann ]
  * Add a patch to make Makefile.PL die if libpcap isn't found.
    (Closes: #633414)

 -- Xavier Guimard <x.guimard@free.fr>  Sat, 25 May 2013 08:23:59 +0200

libnet-pcap-perl (0.16-3) unstable; urgency=low

  [ Franck Joncourt ]
  * Minimized debian/rules:
    + Refreshed debian/rules
    + Updated BD versions on debhelper and quilt to be able to use
     dh --with quilt

  [ gregor herrmann ]
  * debian/control: improve long description, thanks to Peter Pentchev for
    spotting.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Add patch by Niko Tyni to work with perl 5.14 (closes: #629302).
  * Switch to source format 3.0 (quilt); drop quilt framework.
  * Bump debhelper compatibility level to 8.
  * Set Standards-Version to 3.9.2, drop version from perl build dependency.
  * debian/copyright: refresh formatting.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Jul 2011 21:39:19 +0200

libnet-pcap-perl (0.16-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Franck Joncourt ]
  * Do not allow the pcap loop function to be interrupted by signals.
    It happens this causes segfault. (Closes: #508432)
      - Documented the usage of quilt through README.source.
      - Added Build-dependency on quilt in d.control
      - Updated d.rules
      - Added safe_signal.patch.
    This patch makes Net::Pcap behave the same way as its previous release.
  * Set Standards-Version to 3.8.1 (no changes).
  * Removed the created const-xs.inc and const-c.inc during clean.
  * d.copyright: add © symbol for Debian copyright holders.
  * d.control:
    - Add /me to Uploaders.
    - Refresh both short and long descriptions

 -- Franck Joncourt <franck.mail@dthconnex.com>  Wed, 29 Apr 2009 19:46:22 +0200

libnet-pcap-perl (0.16-1) unstable; urgency=low

  * Take over for the Debian Perl Group on maintainer's request
    (http://lists.debian.org/debian-perl/2008/11/msg00047.html).
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Ola Lundqvist
    <opal@debian.org>); Ola Lundqvist <opal@debian.org> moved to
    Uploaders.
  * debian/watch: use dist-based URL.

  * New upstream release (closes: #329635). Includes fixes for
    - "pcap_setnonblock and pcap_getnonblocl implementation" (closes: #270942)
    - "Example/test 07-stats.t fails" (closes: #272869)
  * Remove README.Debian, which just points to a manpage.
  * Don't install README anymore, just a text version of the POD
    documentation.
  * Install "real" examples instead of test scripts.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * Set Standards-Version to 3.8.0 (no changes).
  * debian/copyright: switch to new format.
  * Remove the created funcs.txt during clean.
  * debian/control:
    - build depend on libpcap0.8-dev instead of (transitional) libpcap-dev
    - mention module name in long description
    - add /me to Uploaders
    - add build dependency on libtest-pod-perl to enable an additional test
      (various other tests depend on not existing packages or are not
      functional)
    - change Section to perl

 -- gregor herrmann <gregoa@debian.org>  Thu, 13 Nov 2008 23:33:45 +0100

libnet-pcap-perl (0.04-3) unstable; urgency=low

  * Added directory t from sources to examples directory, closes:
    #203294.
  * Updated standards version to 3.6.1.

 -- Ola Lundqvist <opal@debian.org>  Sat, 14 Aug 2004 22:46:37 +0200

libnet-pcap-perl (0.04-2) unstable; urgency=low

  * Updated standards version from 3.5.2 to 3.5.8.
  * Fixed description line.
  * Maintainer keepup, closes: #158964.
  * Rebuilt using latest libpcap-dev, closes: #156213.

 -- Ola Lundqvist <opal@debian.org>  Fri, 18 Jul 2003 13:54:15 +0200

libnet-pcap-perl (0.04-1.2) unstable; urgency=low

  * NMU: Rebuilt against perl 5.8, closes: #158964.

 -- Ben Burton <benb@acm.org>  Sun,  1 Sep 2002 11:12:15 +1000

libnet-pcap-perl (0.04-1.1) unstable; urgency=low

  * Non maintainer upload
  * Rebuilt with new libpcap to remove dependency on libpcap0, which I
    got removed from unstable by accident. Sorry about this...

 -- Torsten Landschoff <torsten@debian.org>  Sat, 10 Aug 2002 11:37:10 +0200

libnet-pcap-perl (0.04-1) unstable; urgency=low

  * Initial release, closes: #140191.

 -- Ola Lundqvist <opal@debian.org>  Wed, 27 Mar 2002 22:37:04 +0100
